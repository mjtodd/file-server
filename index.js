
var express = require('express')
    app = express(),
    fs = require('fs')

app.use(require('cors')({
  origin: 'http://localhost:3080',
  allowedHeaders: 'Content-Type'
}));


app.use('/files', express.static(__dirname+'/files'))

app.put('/files/:name', function (req, res) {
  var file = fs.createWriteStream(__dirname+'/files/'+req.params.name)
  req.pipe(file)
  file.on('close', function() {
    res.sendStatus(200, "ok")
  })
})

var server = app.listen(process.env.PORT || 3090, function () {

  var host = server.address().address
  var port = server.address().port

  console.log('File server listening at http://%s:%s', host, port)

})
